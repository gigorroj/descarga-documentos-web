import urllib.request


class Robot:

    def __init__(self, _url):
        self.documento = None
        self.url = _url
        self.descargado = False

    def retrieve(self):
        if self.descargado:
            print(f'La url introducida ya se ha descargado anteriormente: {self.url}')
        else:
            f = urllib.request.urlopen(self.url)
            print("Descargando url")
            self.documento = f.read().decode('utf-8')
            self.descargado = True
            print(f'Url descargada:{self.url}')

    def contenido(self):
        self.retrieve()
        return self.documento

    def show(self):
        print(self.contenido())


class Cache:
    def __init__(self):
        self.cache = {}
        self.encache = False

    def retrieve(self, _url):
        if not self.encache:
            robot = Robot(_url)
            self.cache[_url] = robot
            self.encache = True
        else:
            print('La url introducida ya se encuentra descargada y almacenada en la caché')

    def contenido(self, _url):
        self.retrieve(_url)
        return self.cache[_url].contenido()

    def show(self, _url):
        print('Contenido de la url: ')
        print(self.contenido(_url))

    def show_all(self):
        for url in self.cache:
            print(url)



robot1 = Robot('https://gsyc.urjc.es/')
robot1.contenido()
robot1.retrieve()

robot2 = Robot('https://www.aulavirtual.urjc.es')
robot2.contenido()
robot2.retrieve()
robot2.show()


cache1 = Cache()
cache1.retrieve('http://gsyc.urjc.es/')
cache1.contenido('http://gsyc.urjc.es/')
cache1.show('http://gsyc.urjc.es/')
cache1.show_all()

cache2 = Cache()
cache2.retrieve('https://labs.etsit.urjc.es/')
cache2.contenido('https://labs.etsit.urjc.es/')
cache2.show('https://labs.etsit.urjc.es/')
cache2.show_all()
